# Database Wrapper Python
Python 3.7+ Database Wrapper

## Installation
### Pip (Not yet)
Use the package manager [pip](https://pip.pypa.io/en/stable/) to install database-wrapper-python.
```bash
pip install database-wrapper-python
```

### Ubuntu 18.04 (Debian-based Linux)
```bash
python3.8 -m pip install git+ssh://git@gitlab.com/<path>
```

```bash
python3.8 -m pip install git+https://gitlab-ci-token:<personal_access_token>@gitlab.com/<path>
```


### Windows
```bash
pip install git+ssh://git@gitlab.com/<path>
```

```bash
pip install git+https://gitlab-ci-token:<personal_access_token>@gitlab.com/<path>
```

### Requirements.txt
For development of other projects
```text
dbwrapperpy@ git+ssh://git@gitlab.com/team-ernest/database-wrapper-python
```


## Usage
### MySQL Wrapper

```python
import pandas as pd

from dbwrapperpy.mysqldb import MySQLWrapper

db_details = {
    'username': 'user',
    'password': 'password#',
    'host': '127.0.0.1',
    'port': '3306',
    'database': 'db'
}

# Initialisation
mysql_db = MySQLWrapper(db_details=db_details)

# Execute SQL Script.
SQL_SCRIPT = """CREATE TABLE hello_world ..."""
mysql_db.execute_sql(sql_script=SQL_SCRIPT)

# Fetch count.
count = mysql_db.fetch_count(table_name='hello_world')

# Fetch latest insert_timestamp.
latest_insert_timestamp = mysql_db.fetch_latest_insert_timestamp(table_name='hello_world')

# Fetch time series last timestamp.
last_timestamp = mysql_db.fetch_time_series_last_timestamp(table_name='hello_world', column='timestamp')

# Fetch data.
full_df = mysql_db.fetch_data(table_name='hello_world')
partial_df = mysql_db.fetch_count(table_name='hello_world', query="SELECT * FROM helloworld;")

# Push data.
df = pd.DataFrame({'num_legs': [2, 4, 8, 0],
                   'num_wings': [2, 0, 0, 0],
                   'num_specimen_seen': [10, 2, 1, 8]},
                  index=['falcon', 'dog', 'spider', 'fish'])
mysql_db.push_data(table_name='hello_world', data=df)
```

## Contributing
Ernest Yuen


## License
[MIT](https://choosealicense.com/licenses/mit/)
