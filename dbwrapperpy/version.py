"""dbwrapperpy version.

Version uses the Semantic Versioning 2.0.0 - https://semver.org/.
The pre-release and post-release tags used are:
    1. hotfix
    2. dev
    3. pre
    4. rc
"""
__version__ = '0.0.1'
