from setuptools import setup, find_packages

version = {}
with open("dbwrapperpy/version.py") as file:
    exec(file.read(), version)

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="dbwrapperpy",
    version=version['__version__'],
    author="Ernest Yuen",
    author_email="team.ernest.programming@gmail.com",
    description="Python 3.7+ Database Wrapper",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/team-ernest/database-wrapper-python",
    packages=find_packages(),
    install_requires=[
        'greenlet',
        'importlib-metadata',
        'numpy == 1.21.1',
        'pandas == 1.3.1',
        'python-dateutil',
        'pytz',
        'six',
        'SQLAlchemy',
        'typing-extensions',
        'mysql-connector-python',
        'zipp'
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
)
